package com.jala.AtvLab02.dtos;

public record ProductDTO(String name, String description, float price, int quantity){

}