package com.jala.AtvLab02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AtvLab02Application {

	public static void main(String[] args) {
		SpringApplication.run(AtvLab02Application.class, args);
	}

}