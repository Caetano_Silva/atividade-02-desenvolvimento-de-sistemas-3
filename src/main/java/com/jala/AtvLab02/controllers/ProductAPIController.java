package com.jala.AtvLab02.controllers;

import com.jala.AtvLab02.dtos.ProductDTO;
import com.jala.AtvLab02.entities.Product;
import com.jala.AtvLab02.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/product")
public class ProductAPIController {

    private final ProductService productService;

    @Autowired
    public ProductAPIController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping
    public ResponseEntity<Product> createProduct(@RequestBody ProductDTO dto){
        Product newProduct = productService.createProduct(dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(newProduct);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable UUID id){
        productService.deleteProduct(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Product> updateProduct(@PathVariable UUID id, @RequestBody ProductDTO productDTO){
        Product updatedProduct = productService.updateProduct(id, productDTO);
        return ResponseEntity.ok(updatedProduct);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable UUID id){
        Product product = productService.getProductById(id);
        return ResponseEntity.ok(product);
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<Product> getProductByName(@PathVariable String name){
        Product product = productService.getProductByName(name);
        return ResponseEntity.ok(product);
    }

    @GetMapping("/price/{price}")
    public ResponseEntity<List<Product>> getProductByPrice(@PathVariable float price){
        List<Product> products = productService.getProductByPrice(price);
        return ResponseEntity.ok(products);
    }

    @GetMapping("/quantity/{quantity}")
    public ResponseEntity<List<Product>> getProductByQuantity(@PathVariable int quantity){
        List<Product> products = productService.getProductByQuantity(quantity);
        return ResponseEntity.ok(products);
    }

    @GetMapping
    public ResponseEntity<List<Product>> getAllProducts(){
        List<Product> products = productService.getAll();
        return ResponseEntity.ok(products);
    }
}