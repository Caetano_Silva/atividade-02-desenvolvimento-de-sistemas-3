package com.jala.AtvLab02.entities;

import com.jala.AtvLab02.dtos.ProductDTO;
import jakarta.persistence.*;
import lombok.*;
import java.util.UUID;

@Getter
@Setter
@Entity(name= "product")
@Table(name="product")
@NoArgsConstructor
@EqualsAndHashCode(of="id")
public class Product {

  @Id
  @GeneratedValue(strategy = GenerationType.UUID)
  private UUID id;
  private String name;
  private String description;
  private int quantity;
  private float price;


  public Product(ProductDTO productDTO){
    this.name = productDTO.name();
    this.description = productDTO.description();
    this.price = productDTO.price();
    this.quantity = productDTO.quantity();
  }
}
