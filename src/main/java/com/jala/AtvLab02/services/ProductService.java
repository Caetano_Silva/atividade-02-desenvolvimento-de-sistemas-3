package com.jala.AtvLab02.services;

import com.jala.AtvLab02.dtos.ProductDTO;
import com.jala.AtvLab02.entities.Product;
import com.jala.AtvLab02.repositories.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.persistence.EntityNotFoundException;
import java.util.List;
import java.util.UUID;

@Service
public class ProductService {

    @Autowired
    private ProductRepo repository;

    public Product createProduct(ProductDTO dto){
        Product product = new Product(dto);
        return repository.save(product);
    }

    public void deleteProduct(UUID id){
        repository.deleteById(id);
    }

    public List<Product> getAll(){
        return repository.findAll();
    }

    public Product getProductById(UUID id){
        return repository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Product not found"));
    }

    public Product getProductByName(String name){
        return repository.findByName(name)
                .orElseThrow(() -> new EntityNotFoundException("Product not found"));
    }

    public List<Product> getProductByPrice(double price){
        return repository.findAllByPrice(price)
                .orElseThrow(() -> new EntityNotFoundException("Product not found"));
    }

    public List<Product> getProductByQuantity(int quantity){
        return repository.findAllByQuantity(quantity)
                .orElseThrow(() -> new EntityNotFoundException("Products not found with quantity: " + quantity));
    }

    public Product updateProduct(UUID id, ProductDTO productUpdated){
        Product product = repository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Product not found with id: " + id));

        if(productUpdated.name() != null) product.setName(productUpdated.name());
        if(productUpdated.description() != null) product.setDescription(productUpdated.description());
        if(productUpdated.price() > 0) product.setPrice(productUpdated.price());
        if(productUpdated.quantity() > 0) product.setQuantity(productUpdated.quantity());

        return repository.save(product);
    }
}