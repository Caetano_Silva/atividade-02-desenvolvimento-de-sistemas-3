package com.jala.AtvLab02.repositories;

import com.jala.AtvLab02.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProductRepo extends JpaRepository<Product, UUID> {
    Optional<Product> findByName(String name);
    Optional<List<Product>> findAllByPrice(double price);
    Optional<List<Product>> findAllByQuantity(int quantity);
}
